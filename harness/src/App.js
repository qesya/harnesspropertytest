import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <p className="App-intro">
          Harness test
        </p>
      </div>
    );
  }
}

export default App;
